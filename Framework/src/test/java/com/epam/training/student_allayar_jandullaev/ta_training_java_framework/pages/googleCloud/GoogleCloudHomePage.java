package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.googleCloud;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.AbstractPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GoogleCloudHomePage extends AbstractPage {
    private final Logger logger = LogManager.getRootLogger();
    private static final String PAGE_URL = "https://cloud.google.com/";
    private static final String searchItem = "Google Cloud Platform Pricing Calculator legacy";
    @FindBy(xpath = "//span[contains(., '\uE8B6')]/parent::div/parent::div")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@aria-label = 'Search']")
    private WebElement searchInput;
    public GoogleCloudHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public GoogleCloudHomePage openPage() {
        driver.get(PAGE_URL);
        return this;
    }
    public SearchResultPage searchingItem() {
        wait.until(ExpectedConditions.visibilityOf(searchIcon)).click();
        searchInput.sendKeys(searchItem);
        searchInput.sendKeys(Keys.RETURN);
        logger.info("Searched " + searchItem);
        return new SearchResultPage(driver);
    }
}

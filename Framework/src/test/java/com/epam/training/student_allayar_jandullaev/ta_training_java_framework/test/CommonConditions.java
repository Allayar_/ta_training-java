package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.test;

import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.driver.DriverSingleton;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.model.Calculator;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.googleCloud.ComputeEnginePage;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.googleCloud.GoogleCloudHomePage;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.pages.yopmail.YopMailEmailPage;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.service.GoogleCalculatorService;
import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Listeners(TestListener.class)
public class CommonConditions {
    protected static WebDriver driver;
    protected static ComputeEnginePage computeEnginePage;
    protected static YopMailEmailPage yopMailEmailPage;
    Calculator calculator = GoogleCalculatorService.withCredentialsFromProperty();

    @BeforeClass
    public  void setUp() {
        driver = DriverSingleton.getDriver();
        computeEnginePage = new GoogleCloudHomePage(driver)
                .openPage()
                .searchingItem().clickLink().selectComputeEngine()
                .enterNumberOfInstances(calculator.getNumberOfInstances())
                .selectOperatingSystemSoftware()
                .clickProvisioningModel()
                .selectMachineFamily()
                .selectSeries(calculator.getSeries())
                .selectMachineType()
                .clickGPUCheckbox()
                .selectGPUModel(calculator.getGpuType())
                .enterNumberOfGPUs(calculator.getNumberOfGPUs())
                .selectLocalSSD(calculator.getLocalSSD())
                .selectDatacenterLocation()
                .selectCommittedUsage()
                .addToEstimateButton()
                .selectEmailEstimate();
    }

    @AfterClass
    public  void quit() {
        DriverSingleton.quitDriver();
    }
}

package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

public class DriverSingleton {
    private static WebDriver driver;
    public static WebDriver getDriver() {
        if (null == driver) {
            switch (System.getProperty("browser")) {
                case "safari" : {
                    WebDriverManager.safaridriver().setup();
                    driver = new SafariDriver();
                }
                default: {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                }
            }
            driver.manage().window().maximize();
        }
        return driver;
    }
    public static void quitDriver() {
        driver.quit();
        driver = null;
    }
}

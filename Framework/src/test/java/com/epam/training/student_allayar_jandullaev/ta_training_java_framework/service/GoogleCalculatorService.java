package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.service;


import com.epam.training.student_allayar_jandullaev.ta_training_java_framework.model.Calculator;

public class GoogleCalculatorService {
    private static final String TESTDATA_CALC_INSTANCES = "testdata.calculator.instances";
    private static final String TESTDATA_CALC_GPU_TYPE = "testdata.calculator.gpuType";
    private static final String TESTDATA_CALC_NUMBER_OF_GPU = "testdata.calculator.numberOfGpu";
    private static final String TESTDATA_CALC_SERIES = "testdata.calculator.series";
    private static final String TESTDATA_CALC_SSD = "testdata.calculator.ssd";
    public static Calculator withCredentialsFromProperty(){
        return new Calculator(TestDataReader.getTestData(TESTDATA_CALC_INSTANCES),
                TestDataReader.getTestData(TESTDATA_CALC_GPU_TYPE),
                TestDataReader.getTestData(TESTDATA_CALC_NUMBER_OF_GPU),
                TestDataReader.getTestData(TESTDATA_CALC_SERIES),
                TestDataReader.getTestData(TESTDATA_CALC_SSD)
                );
    }


}

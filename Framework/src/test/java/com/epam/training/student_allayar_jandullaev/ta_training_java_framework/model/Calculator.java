package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.model;

import java.util.Objects;

public class Calculator {
    private String numberOfInstances;
    private String gpuType;
    private String numberOfGPUs;
    private String series;
    private String localSSD;

    public Calculator(String numberOfInstances, String gpuType, String numberOfGPUs, String series, String localSSD) {
        this.numberOfInstances = numberOfInstances;
        this.gpuType = gpuType;
        this.numberOfGPUs = numberOfGPUs;
        this.series = series;
        this.localSSD = localSSD;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getGpuType() {
        return gpuType;
    }

    public void setGpuType(String gpuType) {
        this.gpuType = gpuType;
    }

    public String getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public void setNumberOfGPUs(String numberOfGPUs) {
        this.numberOfGPUs = numberOfGPUs;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }
}

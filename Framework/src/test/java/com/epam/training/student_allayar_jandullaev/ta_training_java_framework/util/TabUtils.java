package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.util;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.ArrayList;
public class TabUtils {
    private static ArrayList<String> tabHandles;
    private static final int widthAndHeight = 1000;
    public static void newTab(WebDriver driver) {
        driver.switchTo().newWindow(WindowType.TAB);
        tabHandles = new ArrayList<>(driver.getWindowHandles());
    }
    public static void switchToGooglePage(WebDriver driver) {
        driver.switchTo().window(tabHandles.get(0));
        driver.manage().window().setSize(new Dimension(widthAndHeight, widthAndHeight));
    }
    public static void switchToYopMailPage(WebDriver driver) {
        driver.switchTo().window(tabHandles.get(1));
        driver.manage().window().setSize(new Dimension(widthAndHeight, widthAndHeight));
    }
}

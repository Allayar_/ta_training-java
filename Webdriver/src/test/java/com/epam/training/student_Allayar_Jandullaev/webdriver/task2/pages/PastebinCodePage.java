package com.epam.training.student_Allayar_Jandullaev.webdriver.task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class PastebinCodePage {
    @FindBy(xpath = "//div[@class='info-top']/h1")
    private WebElement title;

    @FindBy(className = "left")
    private WebElement leftTopButtons;

    @FindBy(className = "source")
    private WebElement sourceCode;


    public PastebinCodePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public String getTitle() {
        return title.getText();
    }

    public String getLeftTopButtons() {
        return leftTopButtons.getText();
    }

    public String getSourceCode() {
        return sourceCode.getText().trim();
    }

}

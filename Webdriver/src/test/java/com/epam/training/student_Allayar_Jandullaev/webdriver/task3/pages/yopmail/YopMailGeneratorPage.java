package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.yopmail;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.AbstractPage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class YopMailGeneratorPage extends AbstractPage {
    @FindBy(xpath = "//button[contains(., 'Copy to clipboard')]")
    private WebElement copyToClipboard;

    @FindBy(xpath = "//button[contains(., 'Check Inbox')]")
    private WebElement checkInboxButton;
    public YopMailGeneratorPage(WebDriver driver) {
        super(driver);
        driver.manage().window().setSize(new Dimension(1000, 1000));
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public YopMailGeneratorPage copyToClipboard() {
        wait.until(ExpectedConditions.visibilityOf(copyToClipboard)).click();
        return this;
    }

    public YopMailEmailPage clickCheckInbox() {
        wait.until(ExpectedConditions.visibilityOf(checkInboxButton)).click();
        return new YopMailEmailPage(driver);
    }

}

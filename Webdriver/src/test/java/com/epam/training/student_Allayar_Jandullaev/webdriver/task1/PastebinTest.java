package com.epam.training.student_Allayar_Jandullaev.webdriver.task1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class PastebinTest {
    static WebDriver driver;
    static PastebinHomePage pastebinHomePage;
    @BeforeAll
    public static void setDriver() {
        WebDriverManager.chromedriver().setup();
    }
    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pastebinHomePage = new PastebinHomePage(driver);
        pastebinHomePage.openPage().selectExpirationTenMinutes().inputPasteName();
    }
    @AfterEach
    public void quit() {
      driver.quit();
    }

    @Test
    public void test() {

    }
}

package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.yopmail;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class YopMailHomePage extends AbstractPage {
    private static final String PAGE_URL = "https://yopmail.com/";
    @FindBy(xpath = "//a[@href = 'email-generator']")
    private WebElement randomEmailGenerator;
    public YopMailHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public YopMailHomePage openPage() {
        driver.get(PAGE_URL);
        return this;
    }

    public YopMailGeneratorPage randomEmailGenerator() {
        wait.until(ExpectedConditions.visibilityOf(randomEmailGenerator)).click();
        driver.get(PAGE_URL + "email-generator");
        return new YopMailGeneratorPage(driver);
    }
}

package com.epam.training.student_Allayar_Jandullaev.webdriver.task1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class PastebinHomePage {
    private static final String PAGE_URL = "https://pastebin.com/";
    private static final String PasteName = "helloweb";
    private final WebDriver driver;
    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expiration;

    @FindBy(xpath = "//li[@class='select2-results__option' and text()='10 Minutes']")
    private WebElement selectionTime;

    @FindBy(id = "postform-name")
    private WebElement title;

    public PastebinHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PastebinHomePage openPage() {
        driver.get(PAGE_URL);
        return this;
    }

    public PastebinHomePage selectExpirationTenMinutes() {
        expiration.click();
        selectionTime.click();
        return this;
    }

    public void inputPasteName() {
        title.sendKeys(PasteName);
    }
}
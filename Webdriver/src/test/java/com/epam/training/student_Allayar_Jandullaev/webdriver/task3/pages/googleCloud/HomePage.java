package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.googleCloud;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HomePage extends AbstractPage {
    private static final String PAGE_URL = "https://cloud.google.com/";
    private static final String searchItem = "Google Cloud Platform Pricing Calculator legacy";
    @FindBy(xpath = "//span[contains(., '\uE8B6')]/parent::div/parent::div")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@aria-label = 'Search']")
    private WebElement searchInput;
    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
    public HomePage openPage() {
        driver.get(PAGE_URL);
        return this;
    }
    public SearchResultsPage searchingItem() {
        searchIcon.click();
        searchInput.sendKeys(searchItem);
        searchInput.sendKeys(Keys.RETURN);
        return new SearchResultsPage(driver);
    }
}
package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.utils;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.ArrayList;

public class TabUtils {
    private static ArrayList<String> tabHandles;
    public static void newTab(WebDriver driver) {
        driver.switchTo().newWindow(WindowType.TAB);
        tabHandles = new ArrayList<>(driver.getWindowHandles());
    }
    public static void switchToGooglePage(WebDriver driver) {
        driver.switchTo().window(tabHandles.get(0));
        driver.manage().window().setSize(new Dimension(1000, 1000));
    }
    public static void switchToYopmailPage(WebDriver driver) {
        driver.switchTo().window(tabHandles.get(1));
        driver.manage().window().setSize(new Dimension(1000, 1000));
    }


}

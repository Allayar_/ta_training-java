package com.epam.training.student_Allayar_Jandullaev.webdriver.task2;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task2.pages.PastebinCodePage;
import com.epam.training.student_Allayar_Jandullaev.webdriver.task2.pages.PastebinHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PastebinTest {
    private static final String code = """
             git config --global user.name  "New Sheriff in Town"
             git reset $(git commit-tree HEAD^{tree} -m "Legacy code")
             git push origin master --force""";
    static WebDriver driver;
    static PastebinCodePage page;
    @BeforeAll
    public static void setDriver() {
        WebDriverManager.chromedriver().setup();
    }
    @BeforeAll
    public static void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        page = new PastebinHomePage(driver)
                .openPage()
                .enterCode(code)
                .syntaxHighlighting().selectingBash()
                .pasteExpiration().selectTenMinutes()
                .enterPasteName()
                .createNewPaste();
    }
    @AfterAll
    public static void quit() {
       driver.quit();
    }
    @Test
    public void checkTitle() {
        assertEquals("how to gain dominance among developers", page.getTitle());
    }
    @Test
    public void checkSyntax() {
        assertTrue(page.getLeftTopButtons().contains("Bash"));
    }
    @Test
    public void checkCode() {
        Assertions.assertEquals(code, page.getSourceCode());
    }
}

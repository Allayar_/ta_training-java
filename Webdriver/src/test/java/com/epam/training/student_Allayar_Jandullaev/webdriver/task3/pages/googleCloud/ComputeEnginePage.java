package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.googleCloud;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class ComputeEnginePage extends AbstractPage {
     private static final String REGAXOFCOST = "USD [0-9,\\.]+";
    @FindBy(xpath = "//span[contains(text(), 'Compute Engine')]")
    private WebElement selectComputeEngine;
    @FindBy(xpath = "//input[contains(@aria-label, 'quantity')]")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//md-select-value[contains(., 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)')]")
    private WebElement selectOperatingSystemSoftware;
    @FindBy(xpath = "//md-option[@value = 'free']//div[contains(text(), 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)')]")
    private WebElement operatingSystemItem;
    @FindBy(xpath = "//md-select-value[contains(., 'Regular')]")
    private WebElement selectProvisioningModel;
    @FindBy(xpath = "//md-option[contains(., 'Regular')]")
    private WebElement provisioningModel;
    @FindBy(xpath = "//md-select-value//span//div[contains(., 'General purpose')]")
    private WebElement selectMachineFamily;
    @FindBy(xpath = "//md-option[contains(., 'General purpose')]")
    private WebElement machineFamily;
    @FindBy(xpath = "//md-select-value//span//div[contains(text(), 'E2')]")
    private WebElement selectSeries;
    @FindBy(xpath = "//md-option[contains(., 'N1')]")
    private WebElement series;
    @FindBy(xpath = "//md-select-value//span//div[contains(text(), 'n1-standard-1 (vCPUs: 1, RAM: 3.75GB)')]")
    private WebElement machineType;
    @FindBy(xpath = "//md-option[contains(., 'n1-standard-8 (vCPUs: 8, RAM: 30GB)')]")
    private WebElement machineTypeItem;
    @FindBy(xpath = "//md-checkbox[@aria-label='Add GPUs']")
    private WebElement addGPUCheckbox;
    @FindBy(xpath = "//md-select[contains(., 'GPU type')]")
    private WebElement GPUModel;
    @FindBy(xpath = "//md-option[contains(., 'NVIDIA Tesla V100')]")
    private WebElement GPUModelItem;
    @FindBy(xpath = "//md-select[@placeholder = 'Number of GPUs']")
    private WebElement numberOfGPUs;
    @FindBy(xpath = "//div[contains(@class, 'md-active')]//md-option[@value = '1']")
    private WebElement numberOfGPUsItem;
    @FindBy(xpath = "//md-select[@placeholder = 'Local SSD']")
    private WebElement localSSD;
    @FindBy(xpath = "//md-option[contains(., '2x375 GB')]")
    private WebElement localSSDItem;
    @FindBy(xpath = "//md-select[@placeholder = 'Datacenter location']")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//md-option[@value = 'us-central1']//div[contains(., 'Iowa (us-central1)')]")
    private List<WebElement> datacenterLocationItem;
    @FindBy(xpath = "//md-select[@placeholder = 'Committed usage']")
    private WebElement committedUsage;
    @FindBy(xpath = "//md-option[contains(., '1 Year')]")
    private List<WebElement> committedUsageItem;
    @FindBy(xpath = "//button[@type = 'button' and contains(text(), 'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//button[@title = 'Email Estimate']")
    private WebElement selectEmailEstimate;
    @FindBy(xpath = "//label[contains(., 'Email')]/following-sibling::input")
    private WebElement enterEmail;
    @FindBy(xpath = "//button[contains(., 'Send Email')]")
    private WebElement sendEmailButton;
    @FindBy(xpath = "//*[contains(text(), 'Total Estimated Cost:')]")
    private WebElement checkPrice;
    public ComputeEnginePage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
    }
    public ComputeEnginePage updateDriver(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
        return this;
    }

    public ComputeEnginePage fillComputeEnginePageMethods() {
        selectComputeEngine();
        enterNumberOfInstances();
        selectOperatingSystemSoftware();
        clickProvisioningModel();
        selectMachineFamily();
        selectSeries();
        selectMachineType();
        clickGPUCheckbox();
        selectGPUModel();
        enterNumberOfGPUs();
        selectLocalSSD();
        selectDatacenterLocation();
        selectCommittedUsage();
        addToEstimateButton();
        selectEmailEstimate();
        return this;
    }
    public void selectComputeEngine() {
        selectComputeEngine.click();
    }

    public void enterNumberOfInstances() {
        numberOfInstances.click();
        numberOfInstances.sendKeys("4");
    }

    public void selectOperatingSystemSoftware() {
        selectOperatingSystemSoftware.click();
        wait.until(ExpectedConditions.visibilityOf(operatingSystemItem)).click();
    }

    public void clickProvisioningModel() {
        selectProvisioningModel.click();
        wait.until(ExpectedConditions.visibilityOf(provisioningModel)).click();
    }

    public void selectMachineFamily() {
        selectMachineFamily.click();
        wait.until(ExpectedConditions.visibilityOf(machineFamily)).click();
    }

    public void selectSeries() {
        selectSeries.click();
        wait.until(ExpectedConditions.visibilityOf(series)).click();
    }

    public void selectMachineType() {
        machineType.click();
        wait.until(ExpectedConditions.visibilityOf(machineTypeItem)).click();
    }

    public void clickGPUCheckbox() {
        wait.until(ExpectedConditions.visibilityOf(addGPUCheckbox)).click();
    }

    public void selectGPUModel() {
        GPUModel.click();
        wait.until(ExpectedConditions.visibilityOf(GPUModelItem)).click();
    }

    public void enterNumberOfGPUs() {
        numberOfGPUs.click();
        wait.until(ExpectedConditions.visibilityOf(numberOfGPUsItem)).click();
    }

    public void selectLocalSSD() {
        localSSD.click();
        wait.until(ExpectedConditions.visibilityOf(localSSDItem)).click();
    }

    public void selectDatacenterLocation() {
        datacenterLocation.click();
        wait.until(ExpectedConditions.visibilityOf(datacenterLocationItem.get(0))).click();
    }

    public void selectCommittedUsage() {
        committedUsage.click();
        wait.until(ExpectedConditions.visibilityOf(committedUsageItem.get(0))).click();
    }

    public void addToEstimateButton() {
        addToEstimateButton.click();
    }

    public void selectEmailEstimate() {
        selectEmailEstimate.click();
    }

    public ComputeEnginePage enterEmail() {
        for (int i = 0; i < 1; i++) {
            enterEmail.sendKeys(Keys.TAB);
        }
        enterEmail.sendKeys(Keys.COMMAND + "v");
        return this;
    }
    public ComputeEnginePage sendEmailButton() {
        sendEmailButton.click();
        return this;
    }
    public String getTotalEstimatedCostIsVisible() {
        return wait.until(ExpectedConditions.visibilityOf(checkPrice)).getText();
    }

    public String getTotalEstimatedCost() {
        String costText = wait.until(ExpectedConditions.visibilityOf(checkPrice)).getText();
        Pattern pattern = Pattern.compile(REGAXOFCOST);
        Matcher matcher = pattern.matcher(costText);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new RuntimeException("USD number not found in the cost text");
        }
    }
}

package com.epam.training.student_Allayar_Jandullaev.webdriver.task3;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.googleCloud.ComputeEnginePage;
import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.googleCloud.HomePage;
import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.utils.TabUtils;
import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.yopmail.YopMailEmailPage;
import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.yopmail.YopMailHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoogleCloudTest {
     static WebDriver driver;
     static ComputeEnginePage computeEnginePage;
     static YopMailEmailPage yopmailEmailPage;
     @BeforeAll
     public static void setUp() {
          WebDriverManager.chromedriver().setup();
          driver = new ChromeDriver();
          computeEnginePage = new HomePage(driver)
                  .openPage().searchingItem()
                  .clickLink()
                  .fillComputeEnginePageMethods();
     }
     @AfterAll
     public static void quit() {
           driver.quit();
     }
     @Test
     @Order(1)
     public void testCheckTotalEstimateCost() {
          assertTrue(computeEnginePage.getTotalEstimatedCostIsVisible().contains("Total Estimated Cost: USD"),
                  "Error message if the condition is not met");
     }
     @Test
     @Order(2)
     public void testTotalEstimatedMonthlyCost() throws InterruptedException {
          TabUtils.newTab(driver);
          yopmailEmailPage = new YopMailHomePage(driver).openPage()
                          .randomEmailGenerator().copyToClipboard()
                          .clickCheckInbox();
          TabUtils.switchToGooglePage(driver);
          computeEnginePage = computeEnginePage.updateDriver(driver).enterEmail().sendEmailButton();
          String costInGooglePage = computeEnginePage.getTotalEstimatedCost();
          TabUtils.switchToYopmailPage(driver);
          yopmailEmailPage = yopmailEmailPage.refreshButton();
          String costInYopMailPage = yopmailEmailPage.updateDriver(driver).getEstimatedCost();

          assertEquals(costInYopMailPage,
                 costInGooglePage, "Error: The estimated cost from the email page does not match");
     }
}
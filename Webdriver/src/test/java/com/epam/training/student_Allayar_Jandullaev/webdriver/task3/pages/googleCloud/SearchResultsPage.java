package com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.googleCloud;

import com.epam.training.student_Allayar_Jandullaev.webdriver.task3.pages.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
public class SearchResultsPage extends AbstractPage {
    @FindBy(xpath = "//a[text() = 'Google Cloud Pricing Calculator']")
    private WebElement linkElement;
    public SearchResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
    public ComputeEnginePage clickLink() {
        linkElement.click();
        return new ComputeEnginePage(driver);
    }
}

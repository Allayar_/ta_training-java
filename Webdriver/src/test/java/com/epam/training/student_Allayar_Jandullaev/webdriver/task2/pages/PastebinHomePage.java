package com.epam.training.student_Allayar_Jandullaev.webdriver.task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class PastebinHomePage {
    private static final String PAGE_URL = "https://pastebin.com/";
    private static final String pasteName = "how to gain dominance among developers";
    private final WebDriver driver;
    @FindBy(id = "postform-text")
    private WebElement newPasteTextArea;
    @FindBy(id = "select2-postform-format-container")
    private WebElement syntaxHighlighting;
    @FindBy(xpath = "//li[text()='Bash']")
    private WebElement bash;

    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expiration;

    @FindBy(xpath = "//li[@class='select2-results__option' and text()='10 Minutes']")
    private WebElement selectionTime;

    @FindBy(id = "postform-name")
    private WebElement title;

    @FindBy(xpath = "//button[@class='btn -big' and text()='Create New Paste']")
    private WebElement pasteButton;

    public PastebinHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public PastebinHomePage openPage() {
        driver.get(PAGE_URL);
        return this;
    }

    public PastebinHomePage enterCode(String bashCode) {
        newPasteTextArea.sendKeys(bashCode);
        return this;
    }

    public PastebinHomePage syntaxHighlighting() {
        syntaxHighlighting.click();
        return this;
    }
    public PastebinHomePage selectingBash() {
        bash.click();
        return this;
    }
    public PastebinHomePage pasteExpiration() {
        expiration.click();
        return this;
    }
    public PastebinHomePage selectTenMinutes() {
        selectionTime.click();
        return this;
    }
    public PastebinHomePage enterPasteName() {
        title.sendKeys(pasteName);
        return this;
    }

    public PastebinCodePage createNewPaste() {
        pasteButton.click();
        return new PastebinCodePage(driver);
    }
}
